module Main where
import Data.List
import Data.Maybe
import System.Environment
import System.IO

main = do
       args <- getArgs
       input <- readFile $ args!!0
       let grid = readGrid input
       let stable = stabilizeGridLocal 4 grid
       putStrLn $ show $ countCell Occupied stable
       let stable2 = stabilizeGridLOS 5 grid
       putStrLn $ show $ countCell Occupied stable2

data Cell = Floor | Empty | Occupied
    deriving Eq
type Grid = ([[Cell]], (Int, Int))

inRange :: Ord a => a -> a -> a -> Bool
inRange f c x = (f <= x) && (x <= c)

indices :: [a] -> [Int]
indices xs = map (fromIntegral . (\n -> n-1)) [1..(length xs)]

indices2d  :: (Int, Int) -> [[(Int, Int)]]
indices2d (bx,by)= [ [ (x,y) | x <- [0..bx-1] ] | y <- [0..by-1] ]

showCell :: Cell -> String
showCell c = case c of
        Floor -> "."
        Empty -> "L"
        Occupied -> "#"
instance Show Cell where show = showCell

showMatrix :: Show a => [[a]] -> String
showMatrix [] = ""
showMatrix (x:xs) = (foldl (++) "" (map show x)) ++ (if (null xs) then "" else "\n") ++ showMatrix xs

showGrid :: Grid -> String
showGrid (g,b) = "[" ++ (show b) ++ "\n" ++ (showMatrix g) ++ "]"

readGrid :: String -> Grid
readGrid = 
    let bounds xs = (length $ xs!!0, length $ xs)
        constructGrid xs = ((map.map) readCell xs, bounds xs)
    in constructGrid . lines
    where 
        readCell c =
            case c of
                '.' -> Floor
                'L' -> Empty
                '#' -> Occupied

getGridPos :: Grid -> (Int, Int) -> Maybe Cell
getGridPos (g, (bx,by)) (x,y) = 
    -- bx and by are lengths, they need to be exclusive
    if inRange 0 (bx-1) x && inRange 0 (by-1) y
        then Just $ g !! y !! x
        else Nothing

addVector :: Num a => (a, a) -> (a, a) -> (a, a)
addVector (a,b) (c,d) = (a+c, b+d)

-- Relative local neighbord of a cell (0,0)
localNeighbors :: [(Int, Int)]
localNeighbors =
    let adjaecent = [-1..1]
        self      = (0,0)
    in delete self [ (x,y) | x<- adjaecent, y <- adjaecent ]

absoluteNeighbors :: (Int, Int) -> [(Int, Int)]
absoluteNeighbors pos = map (addVector pos) localNeighbors

-- gridStep f t grid
-- Steps a grid forward in a CA with the parameters
-- f and t
-- where t is the amount of occupied seats a person can tolerate near them
-- and   f is a function (Grid -> (Int Int) -> [(Int, Int)]) which gets a
--         list of coordinates which point to the neighbors this cell should 
--         consider
gridStep :: (Grid -> (Int, Int) -> [(Int, Int)]) -> Int -> Grid -> Grid
gridStep f t (g, b) = ( ((map.map) (cellStep f t (g, b)) (indices2d b)) , b )
    where 
        -- step cell forward within surrounding grid 
        cellStep f t grid pos = 
            -- fromJust turn Just x into x or errors
            -- Here we are only going through positions within bounds
            -- Therefore we will never get Nothing and this is *safe*
            let cell             = fromJust $ getGridPos grid pos
                neighbors        = f grid pos
                neighborOccCount = length $ filter (isOccupied grid) neighbors
            in case cell of
                Empty -> 
                    if neighborOccCount == 0
                        then Occupied
                        else cell
                Occupied ->  
                    if neighborOccCount >= t
                        then Empty
                        else cell
                _ -> cell
            where 
                isOccupied grid pos = 
                    let maybe = getGridPos grid pos
                    -- This case is for walls where neighbors
                    -- do not exist
                    in case maybe of
                        Nothing -> False
                        Just cell -> cell == Occupied

                                
stabilizeGrid :: (Grid -> (Int, Int) -> [(Int, Int)]) -> Int -> Grid -> Grid
stabilizeGrid f t (g,b) = stabilizeGrid' f t (((map.map) (\t -> Floor) $ indices2d b), b) (g,b)
    where  
        stabilizeGrid' f t prev grid =
            if prev == grid
                then grid
                else stabilizeGrid' f t grid $ gridStep f t grid

countCell :: Cell -> Grid -> Int
countCell cell (g,_) = (sum.(map sum)) $ (map.map) (isCell cell) g
    where
        isCell cell x = if cell == x then 1 else 0

-- Local neighbors
neighborFLocal = (\g p -> absoluteNeighbors p)
-- Line of sight neighbors
neighborFLOS :: Grid -> (Int, Int) -> [(Int, Int)]
neighborFLOS grid pos =
    map (lineOfSight pos grid) localNeighbors
    where
        lineOfSight pos grid dir =
            let cpos = addVector pos dir
                cell = getGridPos grid cpos
            in 
                case cell of 
                    Just Floor -> lineOfSight cpos grid dir
                    otherwise -> cpos

stabilizeGridLocal = stabilizeGrid neighborFLocal
stabilizeGridLOS = stabilizeGrid neighborFLOS
