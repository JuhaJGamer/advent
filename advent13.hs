module Main where

import Data.List
import Data.List.Split
import Data.Maybe
import System.IO
import System.Environment

main = do args <- getArgs
          input <- readFile $ args!!0
          let notes = lines input
          let time = read $ notes!!0 :: Integer
          let buses = [ b | Just b <- (map readBus $ splitOn "," $ notes!!1)]
          let next = nextBus buses
          let quickest = next time
          putStrLn $ show $ quickest * (waitLeft quickest time)

readBus :: String -> Maybe Integer
readBus "x" = Nothing
readBus s = Just (read s)

nextBus :: [Integer] -> Integer -> Integer
nextBus bs t = minimumByMap (flip waitLeft t) bs

waitLeft :: Integer -> Integer -> Integer
waitLeft m time = m - mod time m

indices :: [s] -> [Int]
indices xs = [0..((length xs) - 1)]

minimumByMap :: (Ord b) => (a -> b) -> [a] -> a
minimumByMap f = minimumBy (\a b -> compare (f a) (f b))
