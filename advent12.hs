module Main where

import System.IO
import System.Environment
import Debug.Trace

data Movement = Waypoint Integer Integer | Turn Integer | Forward Integer
    deriving Show

type Path = [Movement]

main = do args <- getArgs
          input <- readFile $ args!!0
          let path = readPath input
          putStrLn $ show $ manhattan $ traversePath path

readPath :: String -> Path
readPath = (map readMovement) . lines

-- rounding division
(//) :: (Integral a) => a -> a -> a
(//) a b = floor $ (fromIntegral a) / (fromIntegral b)

readMovement :: String -> Movement
readMovement (dir:n) = 
    let num  = read n :: Integer
        turn = num//90
    in case dir of
        'N' -> Waypoint 0   num
        'S' -> Waypoint 0 (-num)
        'E' -> Waypoint num 0
        'W' -> Waypoint (-num) 0
        'L' -> Turn   turn
        'R' -> Turn (-turn)
        'F' -> Forward num

applyN :: (Integral b) => (a -> a) -> b -> (a -> a)
applyN a 0 = (\a -> a)
applyN a n = a . (applyN a (n-1))

rotate (a,b) = (-b, a)

-- Sum path into a single big movement
traversePath :: Path -> (Integer, Integer)
traversePath = snd . foldl traversePath' ((10,1), (0,0)) 
    where traversePath' ((wx, wy), s) (Waypoint x y) = ((wx+x, wy+y), s)
          traversePath' (wp, s) (Turn n) = ((applyN rotate (mod n 4 + 4)) wp, s)
          traversePath' (wp@(wx, wy), (sx, sy)) (Forward n) = (wp, ((n*wx)+sx, (n*wy)+sy))

-- Manhattan distance
manhattan :: (Integer, Integer) -> Integer
manhattan (a,b) = abs(a) + abs(b)
