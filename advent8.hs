module Main where

import Data.List
import Data.Maybe
import Control.Monad
import Debug.Trace
import Text.Printf
import System.Environment
import System.IO

main = do args <- getArgs
          input <- readFile $ args!!0
          let prog = readProg input
          putStrLn $ show $ evalProgram prog
          putStrLn $ showProgram prog
          let fixed = fromJust $ solveBackward prog
          let diff  = [ x | x <- indices fixed, prog!!x /= fixed!!x ]
          putStrLn $ "Delta: " ++ (show diff)
          putStrLn $ show $ evalProgram fixed

data Op = Acc Int | Nop Int | Jmp Int
    deriving (Eq,Show)

type Program = [Op]

arg :: Op -> Int
arg (Acc n) = n
arg (Nop n) = n
arg (Jmp n) = n

indices :: [a] -> [Int]
indices xs = map (fromIntegral . (\n -> n-1)) [1..(length xs)]

showProgram :: Program -> String
showProgram [] = ""
showProgram (i:is) = (show i) ++ (if is == [] then "" else "\n") ++ (showProgram is)

readInt :: String -> Int
readInt (s:ss) = case s of
    '+' -> read ss
    _   -> read $ s:ss

readProg :: String -> Program
readProg = map readInstruction . lines 
    where 
        readInstruction s =
            let split = words s
                inst = head split
                num = readInt $ last split
            in case inst of
                "acc" -> Acc num
                "nop" -> Nop num
                "jmp" -> Jmp num
                _     -> Nop num

mtrace s [] b = trace s b
mtrace "" (x:xs) b = mtrace (show x) xs b
mtrace s (x:xs) b = mtrace (s ++ " " ++ (show x)) xs b

solveBackward :: Program -> Maybe Program
solveBackward prog = solveBackward' prog (length prog) 1 []
    where
        solveBackward' p x c v =
            let jumpsto x r = case p!!r of
                    Nop n -> n+r == x
                    Jmp n -> n+r == x
                    _     -> False
                next p x c v = case p!!(x-1) of
                    Jmp n     -> tryD p x c v (x-1)
                    otherwise -> solveBackward' p (x-1) c (x:v)
                tryD p x c v r = if c > 0
                    then case p!!r of
                        Jmp n -> delta p x c v r (Nop n)
                        Nop n -> delta p x c v r (Nop n)
                        Acc n -> Nothing
                    else Nothing
                delta p x c v r op =
                    solveBackward' (replace p r op) x (c-1) v
                    where replace p x op = 
                            (take x p) ++ [op] ++ (drop (x+1) p)
                branch p x c v = foldr mplus mzero $ map (tryjump p x v c) (filter (jumpsto x) $ indices p)
                    where tryjump p x v c r = case (p!!r) of
                            Nop n -> tryD p x c v r 
                            Jmp n -> solveBackward' p r c (x:v)
                            Acc _ -> Nothing
            in mtrace ("solve: " ++ (printf "%10s" $ if x < length p then show (p!!x) else "mm")) [x,c] $ case () of 
            _ | x == 0     -> return p
              | x < 0      -> Nothing
              | x `elem` v -> Nothing
              | otherwise  -> mplus (branch p x c v) (next p x c v)

data Evaluation = Hang Int | Halt Int | Error
    deriving Show

evalProgram :: Program -> Evaluation
evalProgram prog =
    evalForward prog 0 0 []
    where 
        evalForward p r a v = 
            let end = length prog
            in case () of 
             _ | r == end   -> Halt a
               | r `elem` v -> Hang a
               | r < 0      -> Error
               | otherwise  -> 
                    case p!!r of
                     Nop _ -> evalForward p (r+1) a     (r:v)
                     Acc n -> evalForward p (r+1) (a+n) (r:v)
                     Jmp n -> evalForward p (r+n) a     (r:v)



