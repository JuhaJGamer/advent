module Main where

import Data.List
import System.IO
import System.Environment
import Debug.Trace

main =  do
        args <- getArgs
        putStrLn $ show args
        input <- readFile (args!!0)
        let adapters = sort $ map readInteger $ lines input
        let connectors = 0 : (adapters ++ [(last adapters) + 3])
        let differences = getJoltageDifferences connectors
        putStrLn $ show differences
        putStrLn $ show $ getConnections connectors (last $ indices connectors) 0

readInteger :: String -> Integer
readInteger = foldl addDigit 0
    where addDigit num d = 10*num +(read [d])

getJoltageDifferences :: [Integer] -> (Integer, Integer)
getJoltageDifferences xs = aux 0 $ sort xs
    where 
        aux :: Integer -> [Integer] -> (Integer, Integer)
        aux _ [] = (0,0)
        aux t (x:xs) = 
            let d = x-t
                p = aux x xs
            in (case d of
                0 -> p
                1 -> ((fst p) + 1, snd p)
                2 -> p
                3 -> (fst p, (snd p) + 1))

indices :: [a] -> [Int]
indices xs = map (\n -> fromIntegral(n-1)) [1..(length xs)]

getConnections :: [Integer] -> Int -> Int -> Int
-- s  : start (index)
-- e  : end (index)
-- xs : list
getConnections xs e s = head $ getConnections' (sort xs) e s
    where getConnections' xs e s =
            let p = getConnections' xs e (s+1)
                c = map (p!!) $ filter (\n -> (xs!!(n+s+1))-(xs!!s) <= 3) $ indices p
            in 
                if s == e
                    then [1]
                    else sum c : p
